variable "gitlab_token" {
  default = "xxx"
  description = "The Personal Access token with API access. THIS MUST BE CHANGED OR YOUR DEMO WILL NOT WORK!."
}

variable "gitlab_url" {
  default = "xx.gitlab.com"
  description = "Full URL GitLab demo environment. THIS MUST BE CHANGED OR YOUR DEMO WILL NOT WORK!"
}
